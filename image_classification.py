import cv2
import numpy as np
from matplotlib import image, pyplot as plt
import dlib
import csv
import os
import pandas as pd
from sklearn.neighbors import KNeighborsClassifier

from sklearn.ensemble import RandomForestClassifier


face_detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_81_face_landmarks.dat")




def initModel():
    data_path = "data.csv"
    ds = pd.read_csv(data_path,usecols = ["forehead", "left-under-eye", "right-under-eye", "left-cheek", "right-cheek", "left-eye-edge", "right-eye-edge", "facial_index",
            "mandibular_index",
            "intercanthal_index",
            "orbital_width_index",
            "eye_fissure_index",
            "nasal_index",
            "vermilion_height_index",
            "mouth_face_with_index",
            "class"])
    X = ds.iloc[:,:15]
    y = ds.iloc[:,-1]
    model=RandomForestClassifier()
    model.fit(X.values,y)
    return model


def feature_extraction(file):   
    img = cv2.imread(file)
    # convert image from RGB -> GRAY 
    convert_img = cv2.cvtColor(src=img, code=cv2.COLOR_BGR2GRAY)
    
    # load face recognition
    faces = face_detector(convert_img)
    
    #get face landmarks
    face_features = predictor(image=convert_img, box=faces[0])

    #canny edge detection
    edge_img = cv2.Canny(convert_img, 45, 60)

    #geomatric feature
    d_en = face_features.part(30).y - face_features.part(27).y
    d_nm = face_features.part(62).y - face_features.part(30).y
    geomatric = d_en/d_nm

    #forehead feature
    forehead = edge_img[face_features.part(71).y:face_features.part(19).y-10, face_features.part(19).x:face_features.part(24).x]
    forehead_wrinkle_percentage = np.sum(forehead>0)*100/(forehead.shape[0]*forehead.shape[1])

    #left under eye feature
    left_under_eye = edge_img[face_features.part(40).y+5:face_features.part(29).y, face_features.part(18).x:face_features.part(21).x]
    left_under_eye_wrinkle_percentage = np.sum(left_under_eye>0)*100/(left_under_eye.shape[0]*left_under_eye.shape[1])

    #right under eye feature
    right_under_eye = edge_img[face_features.part(47).y+5:face_features.part(29).y, face_features.part(22).x:face_features.part(25).x]
    right_under_eye_wrinkle_percentage = np.sum(right_under_eye>0)*100/(right_under_eye.shape[0]*right_under_eye.shape[1])

    #left cheek 
    left_cheek = edge_img[face_features.part(29).y:face_features.part(3).y, face_features.part(3).x:face_features.part(48).x]
    left_cheek_wrinkle_percentage = np.sum(left_cheek>0)*100/(left_cheek.shape[0]*left_cheek.shape[1])

    #right cheek
    right_cheek = edge_img[face_features.part(29).y:face_features.part(13).y, face_features.part(54).x:face_features.part(13).x]
    right_cheek_wrinkle_percentage = np.sum(right_cheek>0)*100/(right_cheek.shape[0]*right_cheek.shape[1])

    #left-eye-edge
    left_eye_edge = edge_img[face_features.part(17).y:face_features.part(29).y, face_features.part(75).x:face_features.part(18).x]
    left_eye_edge_wrinkle_percentage = np.sum(left_eye_edge>0)*100/(left_eye_edge.shape[0]*left_eye_edge.shape[1])

    #right-eye-edge
    right_eye_edge = edge_img[face_features.part(26).y:face_features.part(29).y, face_features.part(25).x:face_features.part(74).x]
    right_eye_edge_wrinkle_percentage = np.sum(right_eye_edge>0)*100/(right_eye_edge.shape[0]*right_eye_edge.shape[1])
    
    facial_index = abs(face_features.part(27).y - face_features.part(8).y) / abs(face_features.part(15).x - face_features.part(1).x)
        
    mandibular_index = abs(face_features.part(66).y - face_features.part(8).y) / abs(face_features.part(12).x - face_features.part(4).x)
    
    intercanthal_index = abs(face_features.part(42).x - face_features.part(39).x) / abs(face_features.part(45).x - face_features.part(36).x)
    
    orbital_width_index = abs(face_features.part(39).x - face_features.part(36).x) / abs(face_features.part(42).x - face_features.part(39).x)
    
    eye_fissure_index = abs(face_features.part(41).y - face_features.part(37).y) / abs(face_features.part(39).x - face_features.part(36).x)
    
    nasal_index = abs(face_features.part(35).x - face_features.part(31).x) / abs(face_features.part(33).y - face_features.part(27).y)
    
    vermilion_height_index = abs(face_features.part(62).y - face_features.part(51).y) / abs(face_features.part(66).y - face_features.part(57).y)
    
    mouth_face_with_index = abs(face_features.part(54).x - face_features.part(48).x) / (face_features.part(15).x - face_features.part(1).x)

    #draw feature zones
    #under eye
    cv2.rectangle(edge_img, (face_features.part(18).x, face_features.part(40).y+5), (face_features.part(21).x, face_features.part(29).y), (255,0,0), 2)
    cv2.rectangle(edge_img, (face_features.part(22).x, face_features.part(47).y+5), (face_features.part(25).x, face_features.part(29).y), (255,0,0), 2)

    #cheek
    cv2.rectangle(edge_img, (face_features.part(3).x, face_features.part(29).y), (face_features.part(48).x, face_features.part(3).y), (255,0,0), 2)
    cv2.rectangle(edge_img, (face_features.part(54).x, face_features.part(29).y), (face_features.part(13).x, face_features.part(13).y), (255,0,0), 2)

    #forehead
    cv2.rectangle(edge_img, (face_features.part(19).x, face_features.part(71).y), (face_features.part(24).x, face_features.part(19).y-10), (255,0,0), 2)

    #left-eye-edge
    cv2.rectangle(edge_img, (face_features.part(75).x, face_features.part(17).y), (face_features.part(18).x, face_features.part(29).y), (255,0,0), 2)

    #right-eye-edge
    cv2.rectangle(edge_img, (face_features.part(25).x, face_features.part(26).y), (face_features.part(74).x, face_features.part(29).y), (255,0,0), 2)
    
    
    return {
        "original_img": img,
        "edge_img": edge_img,
        "features": [
            forehead_wrinkle_percentage,
            left_under_eye_wrinkle_percentage,
            right_under_eye_wrinkle_percentage,
            left_cheek_wrinkle_percentage,
            right_cheek_wrinkle_percentage,
            left_eye_edge_wrinkle_percentage,
            right_eye_edge_wrinkle_percentage,
            facial_index,
            mandibular_index,
            intercanthal_index,
            orbital_width_index,
            eye_fissure_index,
            nasal_index,
            vermilion_height_index,
            mouth_face_with_index,
            ]
    }




